package br.edu.up.puzzle_v1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 09/09/2014
 */
public class ImageSlice {

        public static  Bitmap[] criarSlices(Bitmap image, int rows, int cols)  {

            //File file = new File("bear.jpg"); // I have bear.jpg in my working directory
            //FileInputStream fis = new FileInputStream(file);
            //BufferedImage image = ImageIO.read(fis); //reading the image file
            //Bitmap image = BitmapFactory.decodeFile("ImageD2.jpg");

            //int rows = 3; //You should decide the values for rows and cols variables
            //int cols = 3;
//            int chunks = rows * cols;
//
//            int chunkWidth = image.getWidth() / cols; // determines the chunk width and height
//            int chunkHeight = image.getHeight() / rows;
//            int count = 0;
//            Bitmap imgs[] = new Bitmap[chunks]; //Image array to hold image chunks
//            for (int x = 0; x < rows; x++) {
//                for (int y = 0; y < cols; y++) {
//                    //Initialize the image array with image chunks
//                    imgs[count] = Bitmap.createBitmap(chunkWidth, chunkHeight, Bitmap.Config.RGB_565);
//
//                    // draws the image chunk
//                    // Graphics2D gr = imgs[count++].createGraphics();
//                    //gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
//                    //gr.dispose();
//                }
//            }


            Bitmap[] imgs = new Bitmap[9];
            imgs[0] = Bitmap.createBitmap(image, 0, 0, 240 , 240);
            imgs[1] = Bitmap.createBitmap(image, 240, 0, 240, 240);
            imgs[2] = Bitmap.createBitmap(image, 480, 0, 240,240);
            imgs[3] = Bitmap.createBitmap(image, 0, 240, 240, 240);
            imgs[4] = Bitmap.createBitmap(image, 240, 240, 240,240);
            imgs[5] = Bitmap.createBitmap(image, 480, 240,240,240);
            imgs[6] = Bitmap.createBitmap(image, 0, 480, 240,240);
            imgs[7] = Bitmap.createBitmap(image, 240, 480,240,240);
            imgs[8] = Bitmap.createBitmap(image, 480,480,240,240);
            ///return imgs;
            //System.out.println("Splitting done");

            //writing mini images into image files
            //for (int i = 0; i < imgs.length; i++) {
            //    ImageIO.write(imgs[i], "jpg", new File("img" + i + ".jpg"));
            //}
            //System.out.println("Mini images created");
            return imgs;
        }
}