package br.edu.up.puzzle_v1;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private GridLayout grid;
    private ToggleButton btnSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.grid = (GridLayout) findViewById(R.id.tabuleiro);

        //Bitmap imgPaisagem = BitmapFactory.decodeResource(getResources(), R.drawable.birds);
        Bitmap imgPaisagem = BitmapFactory.decodeResource(getResources(), R.raw.paisagem);

        int widthSlices = imgPaisagem.getWidth() / 720;
        int heigthSlices = imgPaisagem.getHeight() / 720;

        Random r = new Random();
        int x = r.nextInt(widthSlices) * 720;
        int y = r.nextInt(heigthSlices) * 720;

        Bitmap imgOriginal = Bitmap.createBitmap(imgPaisagem, x, y, 720,720);

        Bitmap[] imagens = ImageSlice.criarSlices(imgOriginal, 3, 3);

        int contador = 0;
        ArrayList<View> lista = this.grid.getTouchables();
        this.grid.removeAllViews();

        for (View v : lista){
            ToggleButton btn = (ToggleButton) v;
            Bitmap bmp = imagens[contador];
            BitmapDrawable drawable = new BitmapDrawable(getResources(), bmp);

            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{android.R.attr.state_checked}, drawable);

            Bitmap bmpGray = toGrayscale(bmp);
            BitmapDrawable drawableGray = new BitmapDrawable(getResources(), bmpGray);

            states.addState(new int[]{-android.R.attr.state_checked}, drawableGray);

            btn.setBackgroundDrawable(states);
            btn.setTag(String.valueOf(contador + 1));
            btn.setChecked(false);
            this.grid.addView(btn,240,240);
            contador++;
        }
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public void selecionar(View view) {
        ToggleButton btn = (ToggleButton) view;
        if (!isJogoFinalizado()) {
            if (this.btnSelecionado == null) {
                this.btnSelecionado = btn;
            } else {
                /** O valor da tag precisa ser definida no
                 *  ToggleButton XML de acordo com a posição
                 *  da imagem: Ex.: android:tag="1"
                 */
                String tag = (String) this.btnSelecionado.getTag();
                Drawable img = this.btnSelecionado.getBackground();
                this.btnSelecionado.setBackgroundDrawable(btn.getBackground());
                this.btnSelecionado.setTag(btn.getTag());
                btn.setBackgroundDrawable(img);
                btn.setChecked(false);
                btn.setTag(tag);
                this.btnSelecionado.setChecked(false);
                this.btnSelecionado = null;
            }
            if (isJogoFinalizado()) {
                popularGrid(grid.getTouchables(), true);
            }
        } else {
            btn.setChecked(true);
        }
    }

    public void embaralhar(View view) {
        ArrayList<View> lista = this.grid.getTouchables();
        Collections.shuffle(lista);
        popularGrid(lista, false);
    }

    private boolean isJogoFinalizado(){
        int contador = 1;
        boolean isFinalizado = false;
        for (View v : this.grid.getTouchables()){
            /** O valor da tag precisa ser definida no
             *  ToggleButton XML de acordo com a posição
             *  da imagem: Ex.: android:tag="1"
             */
            int tag = Integer.parseInt(v.getTag().toString());
            if (tag == contador){
                contador++;
            } else {
                break;
            }
        }
        if (contador > 9) {
            isFinalizado = true;
        }
        return isFinalizado;
    }


    private void popularGrid(ArrayList<View> lista, boolean isJogoFinalizado){

        //Limpa todos os botões.
        this.grid.removeAllViews();

        //Converte o valor em DIP para pixels.
        float dips = 120;
        Resources rscs = getResources();
        DisplayMetrics dm = rscs.getDisplayMetrics();
        int unit = TypedValue.COMPLEX_UNIT_DIP;
        int pixels = (int) TypedValue.applyDimension(unit, dips, dm);

        //Popula grid novamente.
        for (View v : lista){
            ToggleButton btn = (ToggleButton) v;
            if (isJogoFinalizado){
                btn.setChecked(true);
            } else {
                btn.setChecked(false);
            }
            this.grid.addView(btn,pixels,pixels);
        }
    }
}